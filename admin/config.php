<?php
// HTTP
define('HTTP_SERVER', 'https://pupsiki.lg.ua/admin/');
define('HTTP_CATALOG', 'https://pupsiki.lg.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://pupsiki.lg.ua/admin/');
define('HTTPS_CATALOG', 'https://pupsiki.lg.ua/');

// DIR
define('DIR_APPLICATION', '/home/p/pupsiki/pupsiki.lg.ua/public_html/admin/');
define('DIR_SYSTEM', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/');
define('DIR_IMAGE', '/home/p/pupsiki/pupsiki.lg.ua/public_html/image/');
define('DIR_LANGUAGE', '/home/p/pupsiki/pupsiki.lg.ua/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/p/pupsiki/pupsiki.lg.ua/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/config/');
define('DIR_CACHE', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/storage/download/');
define('DIR_LOGS', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/p/pupsiki/pupsiki.lg.ua/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/p/pupsiki/pupsiki.lg.ua/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'pupsiki_1');
define('DB_PASSWORD', 'gegcbrb');
define('DB_DATABASE', 'pupsiki_1');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');