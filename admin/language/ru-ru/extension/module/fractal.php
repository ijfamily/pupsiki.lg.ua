<?php
$_['undersell_name']        	= '&nbsp;&nbsp;Нашли дешевле';
$_['calback_name']        	    = '&nbsp;&nbsp;Заказать звонок';
$_['fastorder_name']        	= '&nbsp;&nbsp;Быстрый заказ';
$_['instock_name']        	    = '&nbsp;&nbsp;Сообщить когда появится';
$_['text_istall_theme']        	= '&nbsp;&nbsp;Установка темы';
$_['text_theme_setting']        = '&nbsp;&nbsp;Настройка темы';


$_['text_content_top1']         = 'Баннер справа 1';
$_['text_content_top2']         = 'Баннер справа 2';
$_['text_content_top3']         = 'Баннер середина 1';
$_['text_content_bot1']         = 'Баннер середина 2';
$_['text_content_bot2']         = 'Баннер середина 3';
$_['text_content_bot3']         = 'Баннер середина 4';
$_['text_content_fluid']        = 'Слайдер на всю ширину вверх';
$_['text_content_slleft']       = 'Слайдер слева';
$_['text_content_slrig1']       = 'Баннер на всю ширину середина';
$_['text_content_slrig2']       = 'Позиция после баннер середина ';
$_['text_content_mail']         = 'Подписка на новости';

$_['text_dignity']              = 'Достоинства:';
$_['text_limitations']          = 'Недостатки:';

$_['entry_popupsize']          	= 'Отображать размерную сетку';