<?php
// Heading 
$_['heading_title']   		        = 'News';

// Text
$_['text_error']      		        = 'No News is Good News!';
$_['text_more']  			        = 'Read News';
$_['text_posted'] 		            = 'Posted: ';
$_['text_viewed'] 		            = '(%s views) ';
$_['text_sort']                     = 'Показывать:';

// Buttons
$_['button_news']    	            = 'To News List';
$_['button_continue'] 	            = 'Продолжить';
$_['text_tax']	                    = 'Без налога:';
$_['text_products_related']	        = 'Featured products';
$_['text_aridius_news_related']    	= 'Similar news';
