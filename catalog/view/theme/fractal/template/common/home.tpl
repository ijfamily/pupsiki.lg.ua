<?php echo $header; ?>
<?php echo $content_fluid; ?>
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-8 fixsl"> <?php echo $content_slleft; ?></div>
<div class="col-xs-6 col-md-4 fixslbann"><?php echo $content_top1; ?></div>
<div class="col-xs-6 col-md-4"><?php echo $content_top2; ?></div>
</div>
</div>
<div class="container">
<div class="row">
<?php echo $column_left; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-sm-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-sm-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
</div>
</div>
</div>
<?php if ($content_top3 && $content_bot1 && $content_bot2 && $content_bot3) { ?>
<div class="container">
<div class="row">
<div class="col-xs-6 col-sm-4">
<?php echo $content_top3; ?>
</div>
<div class="middle-banners col-xs-4 hidden-xs">
<?php echo $content_bot1; ?>
<?php echo $content_bot2; ?>
</div>
<div class="col-xs-6 col-sm-4">
<?php echo $content_bot3; ?>
</div>
</div>
</div>
<?php } ?>
<?php if ($content_slrig2) { ?>
<div class="container">
<?php echo $content_slrig2; ?>
</div>
<?php } ?>
<?php if ($content_slrig1) { ?>
<div class="container">
<?php echo $content_slrig1; ?>
</div>
<?php } ?>
<?php if ($content_bottom) { ?>
<div class="container">
<div class="row">
<div id="content2" class="col-sm-12">
<?php echo $content_bottom; ?>
</div>
</div>
</div>
<?php } ?>
<?php echo $footer; ?>