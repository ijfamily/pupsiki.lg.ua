<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/theme/fractal/js/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/fractal/stylesheet/skins/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/fractal/stylesheet/skins/fractal.css" rel="stylesheet">
<link href="catalog/view/theme/fractal/stylesheet/stickers.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
<link href="catalog/view/theme/fractal/js/jquery/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />
<script src="catalog/view/theme/fractal/js/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet">
<link href="catalog/view/theme/fractal/stylesheet/animate.min.css" rel="stylesheet" type="text/css" />
<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
<script src="catalog/view/theme/fractal/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/fractal/js/aridius/aridiusquickview.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>
<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>
<style>
<?php echo $fractal_css; ?>
<?php if ($fractal_description_cat ==1) { ?>	
.product-grid .description_cat{
display: none;	
}
<?php } ?>
<?php if ($fractal_seevmenu_menu !=1) { ?>	
@media (min-width: 992px) {
.hidem{
display: none!important;
	}	
}		
<?php } ?>	
<?php if ($fractal_product_border !=1) { ?>
.product-layout2 {
border: 1px solid #c8c8c8;
}
.product-layout2:hover{
box-shadow: 0 0 7px #c8c8c8;
}
.product-change .product-thumb {
border: 1px solid #c8c8c8;
}
.product-change .product-thumb:hover{
box-shadow: 0 0 7px #c8c8c8;
}
.changebr{
border: 1px solid #9EAAAC
}
.changebr:hover {
box-shadow: 0 0 7px #c8c8c8;
}
<?php } ?>
<?php if ($fractal_logo_middle ==1) { ?>
@media (min-width: 1200px){
#search {
margin: 0 auto; 
}
}
<?php } else { ?>
@media (min-width: 1200px){
#search {
left: 35px;
}
}
<?php } ?>
#top { 
background: #<?php echo $fractal_background_top_menu ?>;
border-bottom: 1px solid #<?php echo $fractal_colorborder_top_menu ?>;
}
#top .btn-link, #top-links a, .tell_nav, #top .btn-lg .caret {
color: #<?php echo $fractal_color_top_menu ?>;
}
#top-links a:hover, #top .btn-link:hover, #top-links li:hover  {
color: #<?php echo $fractal_colorhover_top_menu ?>;
}
#top-menu {
background: #<?php echo $fractal_background_menu ?>;
border-bottom: 1px solid #<?php echo $fractal_colorborder_menu ?>;
border-top: 1px solid #<?php echo $fractal_colorborder_menu ?>;
}
#menu #category, #menu .plus, .plus-link {
color: #<?php echo $fractal_color_menu ?>;
}
#menu .nav > li > a {
border-top: 1px solid #<?php echo $fractal_colorborder_menu ?>;
color: #<?php echo $fractal_color_menu ?>;
margin-top: -2px;
}
#menu .nav > li > a:hover{
color: #<?php echo $fractal_colorhover_menu ?>;
}
li.activetopmenu > a {
color: #<?php echo $fractal_colorhover_menu ?>!important;
}
.top-header, #search .input-lg {
background: #<?php echo $fractal_background_container ?>;
}
#search .form-control::-webkit-input-placeholder { color: #<?php echo $fractal_text_search ?>; }
#search .form-control:-moz-placeholder { color: #<?php echo $fractal_text_search ?>; }
#search .form-control::-moz-placeholder { color: #<?php echo $fractal_text_search ?>; }
#search .form-control:-ms-input-placeholder { color: #<?php echo $fractal_text_search ?>; }
#search .form-control{
border: none;
border-bottom: 1px solid #<?php echo $fractal_color_border_search ?>;
}
#search .btn-lg, #cart > .btn, #search .form-control{
color: #<?php echo $fractal_color_border_search ?>;
}
#menu .nav > li > a:hover, #menu .nav > li.open > a {
border-top: 1px solid #<?php echo $fractal_colorbordertop_menu ?>;
}
.btn-default, .btn-cart, .btn-wishlist, .btn-compare, .btn-quickview, .btn-primary {
background: #<?php echo $fractal_background_btn ?>;
border: 1px solid #<?php echo $fractal_border_btn ?>;
color: #<?php echo $fractal_color_btn ?>;
}
.btncartp {
color: #<?php echo $fractal_color_btn ?>;
}
.btn-default:hover, .btn-cart:hover, .btn-instock:hover, .btn-instock2:hover, .btn-compare:hover, .btn-wishlist:hover, .btn-quickview:hover, .btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled]  {
background: #<?php echo $fractal_backgroundhover_btn ?>;
border: 1px solid #<?php echo $fractal_borderhover_btn ?>;
color: #<?php echo $fractal_colorhover_btn ?>;
}
.cart a:hover, .btn-cart:hover .btncartp {
color: #<?php echo $fractal_colorhover_btn ?>!important;
}
footer {
background: #<?php echo $fractal_footer_background ?>;
}
footer h5, .footer_mailtext {
color: #<?php echo $fractal_footer_h5 ?>;
}
footer .list-unstyled li ,footer .list-unstyled li a, .message_email .textdanger, .message_email .textsuccess {
color: #<?php echo $fractal_footer_h5 ?>;
}
.aboutus_footer, footer .list-unstyled li ,footer .list-unstyled li a {
color: #<?php echo $fractal_footer_colot_text ?>;
}
footer .list-unstyled li a:hover, .mailletters .btn-lg:hover {
color: #<?php echo $fractal_footer_colot_texthover ?>;
}
#mail_letters.form-control{
background: #<?php echo $fractal_footer_background ?>;
color: #<?php echo $fractal_text_search_footer ?>;
}
#mail_letters.form-control{
border: none;
border-bottom: 1px solid #<?php echo $fractal_color_border_search_footer ?>;
}
#mail_letters.form-control::-webkit-input-placeholder { color: #<?php echo $fractal_text_search_footer ?>; }
#mail_letters.form-control:-moz-placeholder { color: #<?php echo $fractal_text_search_footer ?>; }
#mail_letters.form-control::-moz-placeholder { color: #<?php echo $fractal_text_search_footer ?>; }
#mail_letters.form-control:-ms-input-placeholder { color: #<?php echo $fractal_text_search_footer ?>; }
.mailletters .btn-send{
color: #<?php echo $fractal_color_border_search_footer ?>;
}
.circleout {
background-color: #<?php echo $fractal_color_phone ?>;
}
.circle {
box-shadow: 0 0 8px 35px #<?php echo $fractal_color_phone ?>;
}
</style>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129199871-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		
		gtag('config', 'UA-129199871-1');
	</script>
	
</head>
<body class="<?php echo $class; ?>">
<?php if ($fractal_arrowup !=1) { ?>
	<span class="visible-md visible-lg"><a href="#" class="scup"><i class="fa fa-angle-up active"></i></a></span>
	<?php } ?>
<div id="addtocart" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">

</div>
<!-- <div class="no-checkout-message">
<p style="text-align:center; font-size:16px; line-height:30px">
В настоящий момент оформеление заказов через интернет недоступно по техническим причинам.
</p>
<p style="color:red;  text-align:center; font-size:20px; line-height:30px">
Для завершения оформления обратитесь по телефонам  (050)995-40-70, ‎(072)135-01-31. Спасибо за понимание!
</p>
</div> -->
<div class="modal-footer">
<button type="button" class="btn-cart" data-dismiss="modal"><?php echo $text_continue; ?></button>
<a href="<?php echo $checkout; ?>" class="btn-cart"><?php echo $text_checkout; ?></a>
</div>
</div>
</div>
</div>
<div id="wishlist" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
<p></p>
</div>
<div class="modal-footer">
<button type="button" class="btn-cart" data-dismiss="modal"><?php echo $text_continue2; ?></button>
<a href="<?php echo $wishlist; ?>" class="btn-cart"><?php echo $text_wishlist2; ?></a>
</div>
</div>
</div>
</div>
<div id="compare" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
<p></p>
</div>
<div class="modal-footer">
<button type="button" class="btn-cart" data-dismiss="modal"><?php echo $text_continue2; ?></button>
<a href="<?php echo $compare; ?>" class="btn-cart"><?php echo $text_compare2; ?></a>
</div>
</div>
</div>
</div>
<nav id="top">
<div class="container nobackground">
<?php echo $currency; ?>
<?php echo $language; ?>
<?php if ($fractal_top_links3 || $fractal_top_links5 || $fractal_mail_header[$language_id] ) { ?>
	<div class="dropdown phone">
	<div class="listunstyled btn-lg  dropdown-toggle" data-toggle="dropdown"><span class="tell_nav"><i class="fa fa-phone" style="animation: spintell 2s infinite linear;
    -webkit-animation: spintell 2s infinite linear;
    -moz-animation: spintell 2s infinite linear;"></i> <?php echo $fractal_telephone_mainheader[$language_id]; ?></span><b class="caret"></b></div>
	<ul class="dropdown-menu fixxsmenu">
	<?php if ($fractal_top_links3 ) { ?>
		<?php foreach ($fractal_top_links3 as $fractal_top_link3) { ?>
			<li>
			<a <?php if ($fractal_top_link3['title'][$language_id]) { ?> href="tel:<?php echo $fractal_top_link3['title'][$language_id]; ?>"<?php } ?> ><?php if ($fractal_top_link3['faicons_top']) { ?><i class="<?php echo $fractal_top_link3['faicons_top']; ?>"></i><?php } ?> <?php echo html_entity_decode ($fractal_top_link3['title'][$language_id], ENT_QUOTES, 'UTF-8'); ?></a>
			</li>
			<?php } ?>
		<?php } ?>
	<?php if ($fractal_top_links5 ) { ?>
		<?php foreach ($fractal_top_links5 as $fractal_top_link5) { ?>
			<li>
			<a <?php if ($fractal_top_link5['title'][$language_id]) { ?> href="callto:<?php echo $fractal_top_link5['title'][$language_id]; ?>"<?php } ?> ><?php if ($fractal_top_link5['faicons_top']) { ?><i class="<?php echo $fractal_top_link5['faicons_top']; ?>"></i><?php } ?> <?php echo html_entity_decode ($fractal_top_link5['title'][$language_id], ENT_QUOTES, 'UTF-8'); ?></a>
			</li>
			<?php } ?>
		<?php } ?>
	<?php if ($fractal_mail_header[$language_id] ) { ?>
		<li><a onClick="javascript:window.open('mailto:<?php echo $fractal_mail_header[$language_id];?>', 'Mail');event.preventDefault()" ><i class="fa fa-envelope-o "></i> <?php echo $fractal_mail_header[$language_id];?></a></li>
		<?php } ?>
	<li>
	<?php if ($aridiuscallback_status && $fractal_calltop !=1) { ?>
		<a class = "call-order"><?php echo $button_title; ?></a>
		<?php } ?>
	</li>
	</ul>
	</div>
	<?php } else { ?>
	<div class="phone">
	<div class="listunstyled btn-lg"><span class="tell_nav"><?php echo $fractal_telephone_mainheader[$language_id]; ?></span></div>
	</div>
	<?php } ?>
<div id="top-links" class="nav pull-right">
<ul class="list-inline top">
<?php if ($fractal_account_top !=1) { ?><li class="dropdown dropdown-toggle"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" data-toggle="dropdown"><i class="fa fa-user hidden-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <i class="fa fa-caret-down"></i></a>
	<ul class="dropdown-menu dropdown-menu-right">
	<?php if ($logged) { ?>
		<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
		<?php if ($fractal_order_top !=1) { ?><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><?php } ?>
		<?php if ($fractal_transaction_top !=1) { ?> <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><?php } ?>
		<?php if ($fractal_download_top !=1) { ?><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><?php } ?>
		<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
		<?php } else { ?>
		<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
		<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
		<?php } ?>
	</ul>
	</li><?php } ?>
<?php if ($fractal_wishlist_top !=1) { ?><li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart hidden-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li><?php } ?>
<?php if ($fractal_compare_top !=1) { ?><li><a href="<?php echo $compare; ?>" id="compare-total" title="<?php echo $text_compare; ?>"><i class="fa fa-exchange hidden-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_compare; ?></span></a></li><?php } ?>
<?php if ($fractal_cart_top !=1) { ?><li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart hidden-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li><?php } ?>
<?php if ($fractal_checkout_top !=1) { ?> <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share hidden-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li><?php } ?>
<?php if ($fractal_top_links6 ) { ?>
	<?php foreach ($fractal_top_links6 as $fractal_top_link6) { ?>
		<li>
		<a <?php if ($fractal_top_link6['link_top'][$language_id]) { ?> href="<?php echo $fractal_top_link6['link_top'][$language_id]; ?>"<?php } ?> title="<?php echo $fractal_top_link6['title'][$language_id]; ?>"><?php if ($fractal_top_link6['faicons_top']) { ?> <i class="<?php echo $fractal_top_link6['faicons_top']; ?> hidden-lg"></i><?php } ?><?php if ($fractal_top_link6['title']) { ?><span class="hidden-xs hidden-sm hidden-md"> <?php echo $fractal_top_link6['title'][$language_id]; ?></span><?php } ?></a>
		</li>
		<?php } ?>
	<?php } ?>
</ul>
</div>
</div>
</nav>
<header>
<div class="top-header">
<div class="container">
<div class="row">
<?php if ($fractal_logo_middle ==1) { ?>
<div class="col-sm-4 col-md-4">
<div id="logo">
<?php if ($logo) { ?>
		<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
	<?php } else { ?>
	<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
	<?php } ?>
</div>

</div>
<div class="col-sm-4 col-md-5">
<?php echo $search; ?>
<h1 style="font-size:16px; width: 70%;
    margin-left: 35px;" class="">Товары для детей и новорожденных в Луганске</h1>
</div>
<?php } else { ?>
<div class="col-sm-4 col-md-4">
<?php echo $search; ?>
<h1 class="home-pahe-title">Товары для детей и новорожденных в Луганске</h1>
</div>
<div class="col-xs-6 col-sm-4 col-md-5">
<div id="logo">
<?php if ($logo) { ?>
<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
	<?php } else { ?>
	<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
	<?php } ?>
</div>
</div>
<?php } ?>
<div class="col-xs-6 col-sm-4  col-md-3 ">
	<?php echo $cart; ?>
	<div class="gosbank">
		<img src="image/payment/gosbanklnr.jpg" alt="Оплата картой Госбанка ЛНР" class="img-responsive" style="    max-height: 60px;
    margin: 0 auto">
	</div>
</div>
</div>
</div>
</div>
</header>

<div class="section-socseti-banner clearfix">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-vk">
				<a href="https://vk.com/id525582362" target="_blank" title="Все новинки и акции ВКОНТАКТЕ"><img src="/image/vk_button.png" alt="Мы Вконтакте"></a>
			</div>
			
			<div class="col-xs-6 col-insta">
				<a href="https://www.instagram.com/pupsiki.lg.ua/" target="_blank" title="Все новинки и акции в Инстаграм"><img src="/image/insta_button.png" alt="Мы Инстаграм"></a>
			</div>
		</div>
	</div>
</div>

<div class="top-menu">
<div id="top-menu" <?php if ($fractal_sticky_menu !=1) { ?> data-spy="affix" data-offset-top="208" <?php } ?>>
<div class="container nobackground">
<nav id="menu" class="navbar">
<div class="navbar-header"><span id="category" class="visible-xs visible-sm"><?php echo $text_category; ?></span>
<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse">
<ul class="nav navbar-nav">
<?php if ($fractal_ico_home !=1) { ?>
	<li><a href="<?php echo $base; ?>"><i class="fa fa-home" style="font-size: 16px;"></i></a></li>
	<?php } ?>
<?php if ($fractal_ico_home_text !=1) { ?>
	<li><a href="<?php echo $base; ?>"><?php echo $text_homel; ?></i></a></li>
	<?php } ?>
<?php if ($fractal_seevmenu_menu !=1) { ?>
	<li class="dropdown hidev"><a class="with-child dropdown-toggle" <?php if (!empty($fractal_link_menucatalog)) { ?> href="<?php echo $fractal_link_menucatalog; ?>" <?php } ?> data-hover="dropdown" data-delay="1" data-close-others="false">
	<?php if ($fractal_seevmenuico !=1) { ?>
		<i class="fa fa-bars"></i>&nbsp;&nbsp;
		<?php } ?>
	<?php echo $fractal_vmenu_menu[$language_id]; ?><span class="fa fa-angle-down menu"></span></a>
	<ul class="dropdown-menu multi-level" role="menu">
	<?php foreach ($categories as $category) { ?>
		<?php if ($category['children']) { ?>
			<li class="dropdown-submenu" >
			<a style="white-space: normal; margin: 0; padding: 0; left: 12px; position: relative; line-height: 40px;" tabindex="-1" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?><span class="arrowvertmenu"></span>
			<?php if ($fractal_sticker_menu !=1) { ?><?php if ($category['stickers'] == '1') { ?><span class="stickersmenu1v"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '2') { ?><span class="stickersmenu2v"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '3') { ?><span class="stickersmenu3v"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?></a>
			<?php if ($category['image_main'] && $fractal_main_photos_menu !=1) { ?>
				<?php if ($category['column'] < 2) { ?>
					<?php	$column_class = 'col-sm-10'; ?>
					<?php	$column_width = 'column_width1'; ?>
					<?php	$column_clear = 1; ?>
					<?php } elseif ($category['column'] == 2) { ?>
					<?php	$column_class = 'col-sm-4'; ?>
					<?php	$column_width = 'column_width2'; ?>
					<?php	$column_clear = 2; ?>
					<?php } else { ?>
					<?php	$column_class = 'col-sm-3'; ?>
					<?php	$column_width = 'column_width3'; ?>
					<?php	$column_clear = 3; ?>
					<?php } ?>
				<?php } else { ?>
				<?php if ($category['column'] < 2) { ?>
					<?php	$column_class = 'col-sm-12'; ?>
					<?php	$column_width = 'column_width1'; ?>
					<?php	$column_clear = 1; ?>
					<?php } elseif ($category['column'] == 2) { ?>
					<?php	$column_class = 'col-sm-6'; ?>
					<?php	$column_width = 'column_width2'; ?>
					<?php	$column_clear = 2; ?>
					<?php } else { ?>
					<?php	$column_class = 'col-sm-4'; ?>
					<?php	$column_width = 'column_width3'; ?>
					<?php	$column_clear = 3; ?>
					<?php } ?>
				<?php } ?>
			<ul class="dropdown-menu2 <?php echo $column_width; ?>" >
			<?php $i = 0; ?>
			<?php if ($category['image_main'] && $fractal_main_photos_menu !=1) { ?>
				<li><a href="<?php echo $category['href']; ?>"><img class="image_main img-responsive" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" src="<?php echo $category['image_main']; ?>"></a>	</li>
				<?php } ?>
			<?php foreach ($category['children'] as $child) { ?>
				<li class="<?php echo $column_class; ?> mcol">
				<span class = "hidden-xs hidden-sm"><?php if ($fractal_photos_menu !=1 && (!empty($child['image2']))) { ?><a href="<?php echo $child['href']; ?>"><img class="vopmen img-responsive" src="<?php echo $child['image2']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" ></a><?php } ?></span>
				<div class="clearfix"></div>
				<a class="submenu_main" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				<?php if ($fractal_sticker_menu !=1) { ?><?php if ($child['stickers'] == '1') { ?><span class="stickersmenu1v"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '2') { ?><span class="stickersmenu2v"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '3') { ?><span class="stickersmenu3v"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?>
				<?php if (isset($child['children_lv3']) && $child['children_lv3']) { ?>
					<?php $v = 0; ?>
					<?php foreach ($child['children_lv3'] as $child_lv3) { ?>
						<?php $v++; ?>
						<?php if($v >$fractal_countvmenulv) break; ?>
						<a class = "menuv_3lv " href="<?php echo $child_lv3['href']; ?>">&nbsp;&nbsp;- <?php echo $child_lv3['name']; ?></a>
						<?php if ($v >($fractal_countvmenulv -1)) { ?>
							<a class="menuv-allv" href="<?php echo $child['href']; ?>"><?php echo $fractal_seeall_menu[$language_id]; ?></a>
							<?php } ?>
						<?php } ?>
					<?php } ?>
				</li>
				<?php $i++; ?>
				<?php if (($i == $column_clear)&&($i != 1)) { ?>
					<li class="clearfix  visible-md visible-lg"></li>
					<?php $i = 0; ?>
					<?php } ?>
				<?php } ?>
			</ul>
			</li>
			<?php } else { ?>
			<li class="v3hover"><a style="white-space: normal; margin: 0; padding: 0; left: 12px; position: relative; line-height: 40px;" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?>
			<?php if ($fractal_sticker_menu !=1) { ?><?php if ($category['stickers'] == '1') { ?><span class="stickersmenu1v"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '2') { ?><span class="stickersmenu2v"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '3') { ?><span class="stickersmenu3v"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?></a>
			</li>
			<?php } ?>
		<?php } ?>
	</ul>
	</li>
	<?php } ?>
</ul>
<div class="menu_mob_plus">
<div class="hidem">
<ul class="nav navbar-nav">
<?php foreach ($categories as $category) { ?>
	<?php if ($category['children']) { ?>
		<li class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></li>
		<li class="<?php if($category['category_id']==$category_id) {echo 'activetopmenu';}?> dropdown"><a class="with-child dropdown-toggle" href="<?php echo $category['href']; ?>" data-hover="dropdown" data-delay="1" data-close-others="false"><?php echo $category['name']; ?><?php if ($fractal_sticker_menu !=1) { ?><?php if ($category['stickers'] == '1') { ?><span class="stickersmenu1"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '2') { ?><span class="stickersmenu2"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '3') { ?><span class="stickersmenu3"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?><span class="fa fa-angle-down menu"></span></a>
		<div class="dropdown-menu">
		<div class="dropdown-inner children-category">
		<?php if ($category['image_main'] && $fractal_main_photos_menu !=1) { ?>
			<a href="<?php echo $category['href']; ?>"><img class="image_main img-responsive" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" src="<?php echo $category['image_main']; ?>"></a>
			<?php } ?>
		<?php foreach ($category['children'] as $child) { ?>
			<ul class="list-unstyled" style="width: <?php if ($category['image_main'] && $fractal_main_photos_menu !=1) {echo (82/$category['column'])-1;} else {echo (100/$category['column'])-1;} ?>%">
			<?php if (isset($child['children_lv3']) && $child['children_lv3']) { ?>
				<li class="with-child" >
				<span class = "hidden-xs hidden-sm"><?php if ($fractal_photos_menu !=1 && (!empty($child['image2']))) { ?><a href="<?php echo $child['href']; ?>"><img class="opacityhv img-responsive" src="<?php echo $child['image2']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" ></a><?php } ?></span>
				<a href="<?php echo $child['href']; ?>"><span class="style2lv"><?php echo $child['name']; ?><?php if ($fractal_sticker_menu !=1) { ?><?php if ($child['stickers'] == '1') { ?><span class="stickersmenu1v"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '2') { ?><span class="stickersmenu2v"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '3') { ?><span class="stickersmenu3v"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?></span></a></li>
				<?php if ($fractal_3lv_menu !=1) { ?>
					<?php $m = 0; ?>
					<?php foreach ($child['children_lv3'] as $child_lv3) { ?>
						<?php $m++; ?>
						<?php if($m >$fractal_countvmenulv) break; ?>
						<li class="children_lv3"><a href="<?php echo $child_lv3['href']; ?>">&nbsp;&nbsp;- <?php echo $child_lv3['name']; ?></a>
						<?php if ($m >($fractal_countvmenulv -1)) { ?>
							<a class="menuv-all" href="<?php echo $child['href']; ?>"><?php echo $fractal_seeall_menu[$language_id]; ?></a>
							<?php } ?>
						</li>
						<?php } ?>
					<?php } ?>
				<?php } else { ?>
				<li><?php if ($fractal_photos_menu !=1 && (!empty($child['image2']))) { ?><span class = "hidden-xs hidden-sm"><a href="<?php echo $child['href']; ?>"><img class="opacityhv img-responsive" src="<?php echo $child['image2']; ?>" alt="<?php echo $child['name']; ?>" title="<?php echo $child['name']; ?>" ></a></span><?php } ?><a href="<?php echo $child['href']; ?>"><span class="style2lv"><?php echo $child['name']; ?><?php if ($fractal_sticker_menu !=1) { ?><?php if ($child['stickers'] == '1') { ?><span class="stickersmenu1v"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '2') { ?><span class="stickersmenu2v"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($child['stickers'] == '3') { ?><span class="stickersmenu3v"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?></span></a></li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
		</div>
		<div class="clearfix"></div>
		</li>
		<?php } else { ?>
		<li class="<?php if($category['category_id']==$category_id) {echo 'activetopmenu';}?>"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?><?php if ($fractal_sticker_menu !=1) { ?><?php if ($category['stickers'] == '1') { ?><span class="stickersmenu1"><?php echo $fractal_name_sticker_menu_new[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '2') { ?><span class="stickersmenu2"><?php echo $fractal_name_sticker_menu_sale[$language_id];?></span><?php } ?><?php if ($category['stickers'] == '3') { ?><span class="stickersmenu3"><?php echo $fractal_name_sticker_menu_top[$language_id];?></span><?php } ?><?php } ?></a></li>
		<?php } ?>
	<?php } ?>
</ul>
</div>
<ul class="nav navbar-nav">
<!--add menu link-->
<?php if ($fractal_top_links8 ) { ?>
	<?php foreach ($fractal_top_links8 as $fractal_top_link8) { ?>
		<li>
		<a <?php if ($fractal_top_link8['link_top'][$language_id]) { ?> href="<?php echo $fractal_top_link8['link_top'][$language_id]; ?>"<?php } ?>><?php if ($fractal_top_link8['faicons_top']) { ?> <i class="<?php echo $fractal_top_link8['faicons_top']; ?>"></i> <?php } ?><?php if ($fractal_top_link8['title']) { ?><?php echo $fractal_top_link8['title'][$language_id]; ?><?php } ?></a>
		</li>
		<?php } ?>
	<?php } ?>
<!--add menu link-->
<!--other link-->
<?php if ($fractal_main_link_menu[$language_id]) { ?>
	<li class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></li>
	<li class="dropdown"><a class="with-child dropdown-toggle" href="<?php echo $fractal_main_link_href_menu[$language_id]; ?>" data-hover="dropdown" data-delay="1" data-close-others="false"><?php echo $fractal_main_link_menu[$language_id]; ?><span class="fa fa-angle-down menu"></span></a>
	<div class="dropdown-menu">
	<div class="dropdown-inner1 children-category">
	<ul class="list-unstyled">
	<?php if ($fractal_top_links4 ) { ?>
		<?php foreach ($fractal_top_links4 as $fractal_top_link4) { ?>
			<li>
			<a <?php if ($fractal_top_link4['link_top'][$language_id]) { ?> href="<?php echo $fractal_top_link4['link_top'][$language_id]; ?>"<?php } ?> title="<?php echo $fractal_top_link4['title'][$language_id]; ?>"><?php if ($fractal_top_link4['faicons_top']) { ?> <i class="<?php echo $fractal_top_link4['faicons_top']; ?>"></i><?php } ?><?php if ($fractal_top_link4['title']) { ?> <?php echo $fractal_top_link4['title'][$language_id]; ?><?php } ?></a>
			</li>
			<?php } ?>
		<?php } ?>
	</ul>
	</div>
	</div>
	</li>
	<?php } ?>
<!--other link END-->
<!--informations-->
<?php if ($fractal_info_menu !=1) { ?>
	<?php if ($informations) { ?>
		<li class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></li>
		<li class="dropdown"><a class="with-child dropdown-toggle" data-hover="dropdown" data-delay="1" data-close-others="false"><?php echo $text_information; ?><span class="fa fa-angle-down menu"></span></a>
		<div class="dropdown-menu">
		<div class="dropdown-inner1 children-category">
		<ul class="list-unstyled">
		<?php foreach ($informations as $information) { ?>
			<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
			<?php } ?>
		</ul>
		</div>
		</div>
		</li>
		<?php } ?>
	<?php } ?>
<!--informations END-->
<!--manufacturer-->
<?php if ($fractal_man_menu !=1) { ?>
	<?php if ($manufacturers) { ?>
		<li class="plus"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></li>
		<li class="dropdown"><a class="with-child dropdown-toggle" href="<?php echo $href_manufacturer; ?>" data-hover="dropdown" data-delay="1" data-close-others="false"><?php echo $text_manufacturer; ?><span class="fa fa-angle-down menu"></span></a>
		<div class="dropdown-menu">
		<div class="dropdown-inner children-category">
		<?php foreach ($manufacturers as $manufacturer) { ?>
			<ul class="list-unstyled" style="width: <?php echo (100/$fractal_menuman_column)-1; ?>%">
			<li><span class = "hidden-xs hidden-sm "><?php if ($fractal_photos_menu_manuf !=1) { ?><a class = "manufac-menu" href="<?php echo $manufacturer['href']; ?>"><img class="opacityhv manimgmen" src="<?php echo $manufacturer['manufacturer_image']; ?>" alt="<?php echo $manufacturer['name']; ?>" title="<?php echo $manufacturer['name']; ?>" ></a></span><?php } ?><a class = "manufac-menu" href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></li>
			</ul>
			<?php } ?>
		</div>
		</div>
		</li>
		<?php } ?>
	<?php } ?>
<!--manufacturer END-->
</ul>
</div>
</div>
</nav>
</div>
</div></div>
<script type="text/javascript"><!--
$('#menu .menu_mob_plus li').bind().click(function(e) {
	$(this).toggleClass("open").find('>ul').stop(true, true).slideToggle(3000)
	.end().siblings().find('>ul').slideUp().parent().removeClass("open");
	e.stopPropagation();
	// Замена + на - во время открытия меню
	$(this).prev().find('.fa-plus').toggle();
	$(this).prev().find('.fa-minus').toggle();
});
$('#menu li a').click(function(e) {
	e.stopPropagation();
});
// Раскрытие/скрытие пунктов меню 3го уровня
$('.children-category > ul > li').each(function(i, elem){
	if( $(elem).hasClass('children_lv3') ) {
		var ulElements = $(elem).parent().find('li');
		if( $(ulElements[0]).find('a.plus-link').length == 0 ) {
			$(ulElements[0]).append('<a href="#" class="plus-link"><i class="fa fa-plus" style="display: inline; "></i><i class="fa fa-minus" style="display: none;"></i></a>');
			$(ulElements[0]).find('a.plus-link').click(function(e){
				$(ulElements[0]).find('a.plus-link > .fa-plus').toggle();
				$(ulElements[0]).find('a.plus-link > .fa-minus').toggle();
				$(ulElements[0]).parent().find('.children_lv3').toggle();
				return false;
			});
		}
	}
});
//--></script>
<?php if ($fractal_arrowup !=1) { ?>
	<!--scrollUp-->
	<script type="text/javascript"><!--
	$(document).ready(function() {
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scup').fadeIn();
			} else {
				$('.scup').fadeOut();
			}
		});
		$('.scup').click(function(){
			$("html, body").animate({ scrollTop: 0 }, 900);
			return false;
		});
	});
	//--></script>
	<?php } ?>
<!--for sticky-->
<script type="text/javascript"><!--
$(function () {
	if ($(window).width() > 992) {
		onResize();
	}
	function onResize(){
		if ($(window).width() > 992) {
			var heighttopmenu = $("#top-menu").height();
			$('.top-menu').css({position: 'relative',height: heighttopmenu + 2 + 'px'});
		} else {
			$('.top-menu').css({position: 'relative',height: 'auto'});
		}
	}
	window.addEventListener('resize', onResize);
	document.body.addEventListener('resize', onResize);
})
//--></script>