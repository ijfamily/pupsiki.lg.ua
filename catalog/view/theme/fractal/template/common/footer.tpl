<?php if ($fractal_show_fluid_map_footer == '0') { ?>
	<div class="line1"></div>
	<div class="container">
	<div id="map-canvas2" style="position:relative; margin: 0 auto;">
	<?php if ($fractal_show_contactblock == '0') { ?>
		<div class="block-contacts visible-md visible-lg">
		<div class="info-block">
		<div class="footer_contacts">
		<?php echo $text_footer_contacts; ?>
		</div>
		<ul class="fa-ul" style="margin-left;13px;">
		<?php if (!empty($fractal_telephone_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone_footer[$language_id]; ?>"><?php echo $fractal_telephone_footer[$language_id]; ?></a></li>
			<?php } ?>
		<?php if (!empty($fractal_telephone1_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone1_footer[$language_id]; ?>"><?php echo $fractal_telephone1_footer[$language_id]; ?></a></li>
			<?php } ?>
		<?php if (!empty($fractal_telephone2_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone2_footer[$language_id]; ?>"><?php echo $fractal_telephone2_footer[$language_id]; ?></a></li>
			<?php } ?>
		<?php if (!empty($fractal_skype_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-skype"></i><a href="callto:<?php echo $fractal_skype_footer[$language_id]; ?>"><?php echo $fractal_skype_footer[$language_id]; ?></a></li>
			<?php } ?>
		<?php if (!empty($fractal_mail_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-envelope-o "></i><a onClick="javascript:window.open('mailto:<?php echo $fractal_mail_footer[$language_id];?>', 'Mail');event.preventDefault()" ><?php echo $fractal_mail_footer[$language_id];?></a></li>
			<?php } ?>
			<li><br /></li>
		<?php if (!empty($fractal_addres_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-home launch-show"></i><?php echo $fractal_addres_footer[$language_id];?></li>
			<?php } ?>
		<?php if (!empty($fractal_time_footer[$language_id])) { ?>
			<li><i class="fa-li fa fa-check-circle-o "></i><?php echo $fractal_time_footer[$language_id]; ?></li>
			<?php } ?>
		</ul>
		</div>
		</div>
		<?php } ?>
		<div class="block-contacts2"></div>
	</div>
	<div id="map_canvas" style="width:auto; height:320px;position:relative;"></div>
	</div>
	<div class="line2"></div>
	<?php } ?> 
<footer>
<div class="container">
<div class="row">
<div class="col-sm-3 visible-md visible-lg">
<h5><?php echo $text_footer_aboutus; ?></h5>
<div class="aboutus_footer">
<?php echo html_entity_decode($fractal_description[$language_id]); ?>
</div>
</div>
<div class="col-sm-3 visible-xs visible-sm">
<h5><?php echo $text_footer_contacts; ?></h5>
<ul class="list-unstyled liposition fa-ul possm">
<?php if (!empty($fractal_telephone_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone_footer[$language_id]; ?>"><?php echo $fractal_telephone_footer[$language_id]; ?></a></li>
	<?php } ?>
<?php if (!empty($fractal_telephone1_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone1_footer[$language_id]; ?>"><?php echo $fractal_telephone1_footer[$language_id]; ?></a></li>
	<?php } ?>
<?php if (!empty($fractal_telephone2_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-phone"></i><a href="tel:<?php echo $fractal_telephone2_footer[$language_id]; ?>"><?php echo $fractal_telephone2_footer[$language_id]; ?></a></li>
	<?php } ?>
<?php if (!empty($fractal_skype_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-skype"></i><a href="callto:<?php echo $fractal_skype_footer[$language_id]; ?>"><?php echo $fractal_skype_footer[$language_id]; ?></a></li>
	<?php } ?>
<?php if (!empty($fractal_mail_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-envelope-o "></i><a onClick="javascript:window.open('mailto:<?php echo $fractal_mail_footer[$language_id];?>', 'Mail');event.preventDefault()" ><?php echo $fractal_mail_footer[$language_id];?></a></li>
	<?php } ?>
<?php if (!empty($fractal_addres_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-home launch-show"></i><?php echo $fractal_addres_footer[$language_id];?></li>
	<?php } ?>
<?php if (!empty($fractal_time_footer[$language_id])) { ?>
	<li><i class="fa-li fa fa-check-circle-o "></i><?php echo $fractal_time_footer[$language_id]; ?></li>
	<?php } ?>
</ul>
</div>
<div class="col-sm-3">
<h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
		  <?php if ($fractal_contact_footer !=1) { ?><li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li><?php } ?>
        </ul>
</div>
<div class="col-sm-3">
<h5><?php echo $text_account; ?></h5>
<ul class="list-unstyled">
<?php if ($fractal_account_footer !=1) { ?><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><?php } ?>
<?php if ($fractal_order_footer !=1) { ?><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><?php } ?>
<?php if ($fractal_wishlist_footer !=1) { ?><li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li><?php } ?>
<?php if ($fractal_newsletter_footer !=1) { ?><li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li><?php } ?>
<?php if ($fractal_return_footer !=1) { ?><li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li><?php } ?>
</ul>
</div>
<div class="col-sm-3">
<h5><?php echo $text_extra; ?></h5>
<ul class="list-unstyled" >
<?php if ($fractal_manufacturer_footer !=1) { ?><li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li><?php } ?>
<?php if ($fractal_voucher_footer !=1) { ?><li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li><?php } ?>
<?php if ($fractal_affiliate_footer !=1) { ?><li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li><?php } ?>
<?php if ($fractal_sitemap_footer !=1) { ?><li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li><?php } ?>
<?php if ($fractal_special_footer !=1) { ?> <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li><?php } ?>
</ul>
</div>
<div class="clearfix"></div>
<div class="footer_add">
<div class="col-sm-3">
<div class="setifooter_ico">
<?php if ($fractal_top_links2 ) { ?>
	<?php foreach ($fractal_top_links2 as $fractal_top_link2) { ?>
    <a <?php if ($fractal_top_link2['link_top']) { ?> href="<?php echo $fractal_top_link2['link_top']; ?>" target="_blank" <?php } ?>><div data-toggle="tooltip" title="<?php echo $fractal_top_link2['tooltipseti']; ?>" class="setifooter"><i class="diamont_seti <?php echo $fractal_top_link2['faicons_top']; ?>"></i></div></a>
	<?php } ?>
<?php } ?>
</div>
</div>
<div class="clearfix visible-xs"></div>
<div class="col-sm-6">
<?php if (!empty($content_mail)) { ?>
<div class="footer_mailtext"><?php echo $text_footer_mailtext; ?></div>
<?php echo $content_mail; ?>
<?php } ?>
</div>
<div class="col-sm-3">
<?php if (!empty($footer_pay)) { ?>
	<img class="img-responsive rigimg" alt="payment" title="payment" src="<?php echo $footer_pay; ?>">
	<?php } ?>
</div>
</div>
<div class="clearfix"></div>
<div class="copyrightf">
<?php echo html_entity_decode($fractal_license_text[$language_id]); ?>
</div>
<span class="counter-footer">
<?php echo html_entity_decode($fractal_counter); ?>
</span>
<div class="clearfix"></div>
<div class="aboutus_footer visible-xs visible-sm">
<h5><?php echo $text_footer_aboutus; ?></h5>
<?php echo html_entity_decode($fractal_description[$language_id]); ?>
</div>
</div>
</div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<!--код tooltip-->
<script type="text/javascript"><!--
$(document).ready(function(){
	$(".tooltip-examples a").tooltip();
	$(".tooltip-examples button").tooltip();
});
//--></script>
<?php if ($fractal_show_fluid_map_footer == '0') { ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $fractal_fluid_map_key; ?>" type="text/javascript"></script>
<script type="text/javascript"><!--
	function initialize() {
		var latlng = new google.maps.LatLng(<?php echo $fractal_latitude_fluid_map[$language_id]; ?>, <?php echo $fractal_longitude_fluid_map[$language_id]; ?>);
		var settings = {
zoom: <?php echo $fractal_fluid_map_zoom; ?>,
center: {lat: -13, lng: 121},
center: latlng,
zoomControl: true,
scrollwheel: false,
mapTypeControl: false,
navigationControl: true,
navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
mapTypeId: google.maps.MapTypeId.ROADMAP,
   zoomControlOptions: {
        position: google.maps.ControlPosition.BOTTOM_CENTER
    },
   streetViewControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER
    },
styles: [
{
   "featureType": "landscape.natural",
   "elementType": "geometry.fill",
   "stylers": [
   { "color": "#F2F2F2" }
    ]
},
{
   "featureType": "landscape.man_made",
   "stylers": [
   { "color": "#ffffff" },
   { "visibility": "off" }
   ]
},
{
   "featureType": "water",
   "stylers": [
   { "color": "#B4E5F0" },
   { "saturation": 0 }
   ]
},
{
   "featureType": "road.arterial",
   "elementType": "geometry",
   "stylers": [
   { "color": "#FDFDFD" }
    ]
}
,{
   "elementType": "labels.text.stroke",
   "stylers": [
   { "visibility": "off" }
  ]
}
,{
   "elementType": "labels.text",
   "stylers": [
   { "color": "#333333" }
   ]
}
,{
   "featureType": "poi",
   "stylers": [
   { "visibility": "off" }
   ]
}
]
//------------конец --------------
};
		var map = new google.maps.Map(document.getElementById("map_canvas"), settings);
		var contentString = '<div id="content">'+
		'<div id="siteNotice">'+
		'</div>'+
		'<div id="bodyContent">'+
		'<?php echo html_entity_decode($fractal_description_map_logo[$language_id]); ?>'+
		'</div>'+
		'</div>';
		var infowindow = new google.maps.InfoWindow({
content: contentString
		});
		var companyImage = new google.maps.MarkerImage('<?php echo $map_img; ?>',
		new google.maps.Size(80,80),
		new google.maps.Point(0,0),
		new google.maps.Point(50,50)
		);
		var companyMarker = new google.maps.Marker({
position: latlng,
map: map,
icon: companyImage,
zIndex: 3});
		<?php if ($fractal_show_info_marker_block !=1) { ?>
			google.maps.event.addListener(companyMarker, 'mouseover', function() {
				infowindow.open(map,companyMarker);
			});
			<?php } ?>	
	}
	google.maps.event.addDomListener(window, 'load', initialize);
//--></script>
	<?php } ?>
<div class="text-center">
<!-- Yandex.Metrika informer --> <a href="https://metrika.yandex.ru/stat/?id=50818324&amp;from=informer" target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/50818324/1_0_FFFFFFFF_EFEFEFFF_0_uniques" style="width:80px; height:15px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (уникальные посетители)" /></a> <!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter50818324 = new Ya.Metrika2({ id:50818324, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/50818324" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</div>


</body></html>