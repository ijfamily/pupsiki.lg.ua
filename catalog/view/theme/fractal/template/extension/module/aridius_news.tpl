<?php if ($aridius_news) { ?>
<h3 class="title_h title_center"><?php echo $heading_title; ?></h3>
<?php if ($showbutton) { ?>
        <div class="pull-center"><a href="<?php echo $aridius_newslist; ?>"><?php echo $buttonlist; ?></a></div>
<?php } ?>
<div class="row mt">
  <?php foreach ($aridius_news as $aridius_news_story) { ?>	
   <?php if ($aridius_news_home_limit == 2) { ?>
	 <?php $class = 'product-layoutcat col-lg-6 col-md-6 col-sm-6 col-xs-12'; ?>
    <?php } elseif ($aridius_news_home_limit == 3) { ?>
	 <?php $class = 'product-layoutcat col-lg-4 col-md-3 col-sm-6 col-xs-12'; ?>
    <?php } else { ?>
	 <?php $class = 'product-layoutcat col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
    <?php } ?>
<div class="<?php echo $class; ?>">
     <?php if ($aridius_news_story['image'] && !$aridius_news_show_img) { ?>	
 <div class="grid-aridius_news">
        <a href="<?php echo $aridius_news_story['href']; ?>" title="<?php echo $aridius_news_story['title']; ?>"><img class="img-responsive aridius_news-hover" src="<?php echo $aridius_news_story['image']; ?>" alt="<?php echo $aridius_news_story['title']; ?>" /></a>				
<?php if (!$aridius_news_show_date) { ?>			
		<p><span class="posted-aridius_newshome"><i class="fa fa-clock-o"></i> <?php echo $aridius_news_story['posted']; ?></span></p>
<?php } ?>		
</div>	
<?php } ?>	
<div class="aridius_news-caption">
<div class="item-title">	
		<h4><a href="<?php echo $aridius_news_story['href']; ?>"><?php echo $aridius_news_story['title']; ?></a></h4>
	</div>	
<?php if (!$aridius_news_show_description) { ?>
<div class="description-aridius_news">	
        <p><?php echo $aridius_news_story['description']; ?></p>
</div>			
<?php } ?>		
<div class="read-more">		
		<a href="<?php echo $aridius_news_story['href']; ?>"><?php echo $text_more; ?></a>
</div>		
</div>	
     </div>
  <?php } ?>			
</div>
	<?php } ?>