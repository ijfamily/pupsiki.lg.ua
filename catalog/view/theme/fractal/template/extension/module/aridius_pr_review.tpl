<h3 class="title_h title_center"><?php echo $heading_title; ?></h3>
<div class="padding-carusel row">
<div id="rev_carusel<?php echo $module; ?>" class="owl-carousel">

  <?php foreach ($products as $product) { ?>   

  <div class="product-layout2">
<div class="product-thumb transition">
     
	<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
			
	  
<div class="caption">
<h4><a href="<?php echo $product['href']; ?>">
<?php  
if( strlen($product['name'] ) < $fractal_limit_symbolst) { 
echo $product['name']; 
 } 
else {
echo mb_substr( $product['name'],0,$fractal_limit_symbolst,'utf-8' )."..."; } 
?>
</a></h4>
<p><?php echo $product['text']; ?></p>
<?php if ($rat) { ?>		
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
<?php } ?>		
      </div>
<div class="cart">
<a class="btn-cart" href="<?php echo $product['href']; ?>" role="button"><?php echo $button_more; ?></a>
</div>
	<div class="effect-hover">
	<p>
<?php if ($wish) { ?>	
	<a class="wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-fw fa-heart"></i></a>
<?php } ?>	
<?php if ($comp) { ?>	
	<a class="compare" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-fw fa-exchange"></i></a>
<?php } ?>
<?php if ($quickview) { ?>	
<a class="quickview" data-toggle="tooltip" title="<?php echo $button_quickview; ?>" onclick="quickview_open('<?php echo $product['product_id']; ?>');"><i class="fa fa-fw fa-search"></i></a>
<?php } ?>
	</p>
	</div>
    </div>
	</div>
  <?php } ?>
  </div>  </div>
<script>
$(document).ready(function() {
$('#rev_carusel<?php echo $module; ?>').owlCarousel({
items : <?php echo $items; ?>, 
<?php if ($items == '1') { ?>
itemsTablet: [1199,1],
<?php } ?>   
autoPlay: <?php echo $autoplay; ?>, 
<?php if ($navigation) { ?>
navigation: true,
<?php } ?>
<?php if (!$pagination) { ?>
pagination: false,
<?php } ?>
navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
<?php if ($stophover) { ?>         
stopOnHover: true,
<?php } ?>
paginationSpeed: <?php echo $pag_speed; ?>, 
rewindSpeed: <?php echo $rew_speed; ?> 
});
});
</script>

  
