<h3 class="title_h title_center"><?php echo $heading_title; ?></h3>
<div class="row catwall">
    <?php foreach ($categories as $category) { ?>
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 ">
<div class="product-layoutwall2">
		    <div class="namelink">
            <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
            </div>
<?php if ($add_img == '1') { ?>			
            <div class="product-thumb transition">
                <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
            </div>	
<?php } ?>
			<?php if ($child_visible) { ?>
			<?php if ($category['children']) { ?>
					<ul class="list-unstyled rightwallchild">
            <?php foreach ($category['children'] as $child) { ?>
			        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
            <?php } ?>
				   <li><span><a href="<?php echo $category['href']; ?>"><?php echo $Seeall; ?></a></span></li>
              </ul>
				     <?php } ?>
			<?php } ?>		 
</div>
     </div>		
    <?php } ?>
</div>