<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

            $data['aridiusfastorder'] = $this->load->controller('module/aridiuscallback');
			

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['robots'] = $this->document->getRobots();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

                    $this->load->language('extension/module/fractal');
					
      

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		 $data['checkout'] = $this->url->link('checkout/onepagecheckout', '', true);	
		$data['contact'] = $this->url->link('information/contact');

           $data['button_title'] = $this->language->get('button_title');
		   $data['aridiuscallback_status'] = $this->config->get('aridiuscallback_status');
			
		$data['telephone'] = $this->config->get('config_telephone');

	  		        $data['compare'] = $this->url->link('product/compare');
			        $data['searchlink'] = $this->url->link('product/search');
	             	$data['text_information'] = $this->language->get('text_information');
	            	$data['text_contact'] = $this->language->get('text_contact');
					$data['text_manufacturer'] = $this->language->get('text_manufacturer');
					$data['text_continue'] = $this->language->get('text_continue');					
					$data['text_continue2'] = $this->language->get('text_continue2');					
					$data['text_wishlist2'] = $this->language->get('text_wishlist2');
					$data['text_compare2'] = $this->language->get('text_compare2');
			        $data['href_manufacturer'] = $this->url->link('product/manufacturer');
					$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));	
                    $data['button_compare'] = $this->language->get('button_compare');
					$data['text_homel'] = $this->language->get('text_homel');
      

		// Menu
		$this->load->model('design/custommenu');
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

					$this->load->model('tool/image');
      

		$data['categories'] = array();
		
		if ($this->config->get('configcustommenu_custommenu')) {
		$custommenus = $this->model_design_custommenu->getcustommenus();
        $custommenu_child = $this->model_design_custommenu->getChildcustommenus();

        foreach($custommenus as $id => $custommenu) {
			$children_data = array();
        
			foreach($custommenu_child as $child_id => $child_custommenu) {
                if (($custommenu['custommenu_id'] != $child_custommenu['custommenu_id']) or !is_numeric($child_id)) {
                    continue;
                }

                $child_name = '';

                if (($custommenu['custommenu_type'] == 'category') and ($child_custommenu['custommenu_type'] == 'category')){
                    $filter_data = array(
                        'filter_category_id'  => $child_custommenu['link'],
                        'filter_sub_category' => true
                    );

                    $child_name = ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '');
                }

                $children_data[] = array(
                    'name' => $child_custommenu['name'] . $child_name,
                    'href' => $this->getcustommenuLink($custommenu, $child_custommenu)
                );
            }

			$data['categories'][] = array(

					'top'     => $category['top'],
					'category_id' => $category['category_id'],

			        'image_main' => $image_main,
      
      
				'name'     => $custommenu['name'] ,
				'children' => $children_data,
				'column'   => $custommenu['columns'] ? $custommenu['columns'] : 1,
				'href'     => $this->getcustommenuLink($custommenu)
			);
        }
		
		} else {

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {

					if ($category['image']) {
					$image_main = $this->model_tool_image->resize($category['image'],$this->config->get('fractal_photos_width_image_main'), $this->config->get('fractal_photos_height_image_main'));
					} else {		
					$image_main = '';
					};
      
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {

					$children_lv3_data = array();
					$children_lv3 = $this->model_catalog_category->getCategories($child['category_id']);
					foreach ($children_lv3 as $child_lv3) {
					$filter_data_lv3 = array(
					'filter_category_id'  => $child_lv3['category_id'],
					'filter_sub_category' => true
					);		
					$children_lv3_data[] = array(
					'name'  => $child_lv3['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data_lv3) . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_lv3['category_id'])
					);
						}	
					if ($child['image']) {
					$img_lv2 = $this->model_tool_image->resize($child['image'],$this->config->get('fractal_photos_menu_width'), $this->config->get('fractal_photos_menu_height'));
					} else {		
					$img_lv2 = '';
					};
      
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),

					'stickers'     => $child['stickers'],	
      

	                'stickers'     => $child['stickers'],
					'children_lv3' => $children_lv3_data,
		            'column'   => $child['column'] ? $child['column'] : 1,
		            'image2' => $img_lv2,
      
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(

					'top'     => $category['top'],
					'category_id' => $category['category_id'],

			        'image_main' => $image_main,
      
      
					'name'     => $category['name'],

					'stickers'     => $category['stickers'],	
      
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
		
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		if ($this->config->get('configblog_blog_menu')) {
			$data['menu'] = $this->load->controller('blog/menu');
		} else {
			$data['menu'] = '';
		}
		$data['search'] = $this->load->controller('common/search');

					if (isset($this->request->get['path'])) {
						$parts = explode('_', (string)$this->request->get['path']);
					} else {
						$parts = array();
					}
					if (isset($parts[0])) {
						$data['category_id'] = $parts[0];
					} else {
						$data['category_id'] = 0;
			        }
					
					$this->load->model('catalog/information');
					$this->load->language('common/footer');
					$this->load->model('catalog/manufacturer');
					$data['informations'] = array();
					foreach ($this->model_catalog_information->getInformations() as $result) {
						if ($result['bottom']) {
							$data['informations'][] = array(
								'title' => $result['title'],
								'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
							);
						}
					}
			        $data['manufacturers'] = array();
			        $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
			     	foreach($manufacturers as $manufacturer){
					if ($manufacturer['image']) {
		             $img_manufacturer = $this->model_tool_image->resize($manufacturer['image'],$this->config->get('fractal_photos_menuman_width'), $this->config->get('fractal_photos_menuman_height'));
		            	} else {		
		            $img_manufacturer = $this->model_tool_image->resize('placeholder.png', 90, 90);
		                };	
					if($manufacturer){
				      $data['manufacturers'][] = array(
						'name' => $manufacturer['name'],
						'manufacturer_image' => $img_manufacturer,
						'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id='.$manufacturer['manufacturer_id'])
					);
				       }
				          }
      
		$data['cart'] = $this->load->controller('common/cart');

		            $data['language_id'] = $this->config->get('config_language_id');
		            $data['fractal_account_top'] = $this->config->get('fractal_account_top');
					$data['fractal_top_links3'] = $this->config->get('fractal_top_links3');
					$data['fractal_top_links4'] = $this->config->get('fractal_top_links4');
					$data['fractal_top_links5'] = $this->config->get('fractal_top_links5');
					$data['fractal_top_links6'] = $this->config->get('fractal_top_links6');
					$data['fractal_top_links8'] = $this->config->get('fractal_top_links8');
					$data['fractal_telephone_top'] = $this->config->get('fractal_telephone_top');
					$data['fractal_wishlist_top'] = $this->config->get('fractal_wishlist_top');
					$data['fractal_compare_top'] = $this->config->get('fractal_compare_top');
					$data['fractal_cart_top'] = $this->config->get('fractal_cart_top');
					$data['fractal_checkout_top'] = $this->config->get('fractal_checkout_top');
					$data['fractal_calltop'] = $this->config->get('fractal_calltop');
					$data['fractal_order_top'] = $this->config->get('fractal_order_top');
					$data['fractal_transaction_top'] = $this->config->get('fractal_transaction_top');
					$data['fractal_download_top'] = $this->config->get('fractal_download_top');
					$data['fractal_telephone_mainheader'] = $this->config->get('fractal_telephone_mainheader');
					$data['fractal_mail_header'] = $this->config->get('fractal_mail_header');
				    $data['fractal_sticky_menu'] = $this->config->get('fractal_sticky_menu');
					$data['fractal_photos_menu'] = $this->config->get('fractal_photos_menu');
					$data['fractal_photos_menu_manuf'] = $this->config->get('fractal_photos_menu_manuf');
					$data['fractal_3lv_menu'] = $this->config->get('fractal_3lv_menu');
					$data['fractal_info_menu'] = $this->config->get('fractal_info_menu');
					$data['fractal_man_menu'] = $this->config->get('fractal_man_menu');
					$data['fractal_main_link_menu'] = $this->config->get('fractal_main_link_menu');
					$data['fractal_main_link_href_menu'] = $this->config->get('fractal_main_link_href_menu');
					$data['fractal_sticker_menu'] = $this->config->get('fractal_sticker_menu');
					$data['fractal_name_sticker_menu_new'] = $this->config->get('fractal_name_sticker_menu_new');
					$data['fractal_name_sticker_menu_top'] = $this->config->get('fractal_name_sticker_menu_top');
					$data['fractal_name_sticker_menu_sale'] = $this->config->get('fractal_name_sticker_menu_sale');
					$data['fractal_arrowup'] = $this->config->get('fractal_arrowup');
				    $data['fractal_logo_middle'] = $this->config->get('fractal_logo_middle');
				    $data['fractal_product_border'] = $this->config->get('fractal_product_border');
					$data['fractal_callleft'] = $this->config->get('fractal_callleft');
			        $data['fractal_menuman_column'] = $this->config->get('fractal_menuman_column');
					$data['fractal_css'] = $this->config->get('fractal_css');
					$data['fractal_main_photos_menu'] = $this->config->get('fractal_main_photos_menu');
					$data['fractal_ico_home'] = $this->config->get('fractal_ico_home');
					$data['fractal_vmenu_menu'] = $this->config->get('fractal_vmenu_menu');
					$data['fractal_seeall_menu'] = $this->config->get('fractal_seeall_menu');
					$data['fractal_countvmenulv'] = $this->config->get('fractal_countvmenulv');
					$data['fractal_seevmenuico'] = $this->config->get('fractal_seevmenuico');
					$data['fractal_seevmenu_menu'] = $this->config->get('fractal_seevmenu_menu');
					$data['fractal_link_menucatalog'] = $this->config->get('fractal_link_menucatalog');
					$data['fractal_description_cat'] = $this->config->get('fractal_description_cat');
					$data['fractal_ico_home_text'] = $this->config->get('fractal_ico_home_text');
					$data['fractal_background_top_menu'] = $this->config->get('fractal_background_top_menu');
					$data['fractal_color_top_menu'] = $this->config->get('fractal_color_top_menu');
					$data['fractal_colorhover_top_menu'] = $this->config->get('fractal_colorhover_top_menu');
					$data['fractal_colorborder_top_menu'] = $this->config->get('fractal_colorborder_top_menu');
					$data['fractal_background_menu'] = $this->config->get('fractal_background_menu');
					$data['fractal_color_menu'] = $this->config->get('fractal_color_menu');
					$data['fractal_colorhover_menu'] = $this->config->get('fractal_colorhover_menu');
					$data['fractal_colorborder_menu'] = $this->config->get('fractal_colorborder_menu');
					$data['fractal_colorbordertop_menu'] = $this->config->get('fractal_colorbordertop_menu');
					$data['fractal_background_container'] = $this->config->get('fractal_background_container');
					$data['fractal_text_search'] = $this->config->get('fractal_text_search');
					$data['fractal_color_border_search'] = $this->config->get('fractal_color_border_search');
				    $data['fractal_background_btn'] = $this->config->get('fractal_background_btn');
					$data['fractal_backgroundhover_btn'] = $this->config->get('fractal_backgroundhover_btn');
					$data['fractal_border_btn'] = $this->config->get('fractal_border_btn');				
					$data['fractal_borderhover_btn'] = $this->config->get('fractal_borderhover_btn');	
					$data['fractal_color_btn'] = $this->config->get('fractal_color_btn');	
				    $data['fractal_colorhover_btn'] = $this->config->get('fractal_colorhover_btn');
                    $data['fractal_footer_background'] = $this->config->get('fractal_footer_background');					
	                $data['fractal_footer_h5'] = $this->config->get('fractal_footer_h5');				
                    $data['fractal_footer_colot_text'] = $this->config->get('fractal_footer_colot_text');					
	                $data['fractal_footer_colot_texthover'] = $this->config->get('fractal_footer_colot_texthover');	
                    $data['fractal_text_search_footer'] = $this->config->get('fractal_text_search_footer');	
                    $data['fractal_color_border_search_footer'] = $this->config->get('fractal_color_border_search_footer');	  
		            $data['fractal_color_phone'] = $this->config->get('fractal_color_phone');	  			
      

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		return $this->load->view('common/header', $data);
	}
	
	public function getcustommenuLink($parent, $child = null) {
		 if ($this->config->get('configcustommenu_custommenu')) {
        $item = empty($child) ? $parent : $child;

        switch ($item['custommenu_type']) {
            case 'category':
                $route = 'product/category';

                if (!empty($child)) {
                    $args = 'path=' . $parent['link'] . '_' . $item['link'];
                } else {
                    $args = 'path='.$item['link'];
                }
                break;
            case 'product':
                $route = 'product/product';
                $args = 'product_id='.$item['link'];
                break;
            case 'manufacturer':
                $route = 'product/manufacturer/info';
                $args = 'manufacturer_id='.$item['link'];
                break;
            case 'information':
                $route = 'information/information';
                $args = 'information_id='.$item['link'];
                break;
            default:
                $tmp = explode('&', str_replace('index.php?route=', '', $item['link']));

                if (!empty($tmp)) {
                    $route = $tmp[0];
                    unset($tmp[0]);
                    $args = (!empty($tmp)) ? implode('&', $tmp) : '';
                }
                else {
                    $route = $item['link'];
                    $args = '';
                }

                break;
        }

        $check = stripos($item['link'], 'http');
        $checkbase = strpos($item['link'], '/');
        if ( $check === 0 || $checkbase === 0 ) {
			$link = $item['link'];
        } else {
            $link = $this->url->link($route, $args);
        }
        return $link;
    }
	}
}
