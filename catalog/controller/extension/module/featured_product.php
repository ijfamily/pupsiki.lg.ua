<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerExtensionModuleFeaturedProduct extends Controller {
	public function index($setting) {

					static $module = 0;	
      
		
		
		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
		
		$results = array();
		
		$this->load->model('catalog/cms');
		
			if (isset($this->request->get['manufacturer_id'])) {
					
					$filter_data = array(
						'manufacturer_id'  => $this->request->get['manufacturer_id'],
						'limit' => $setting['limit']
					);
					
					$results = $this->model_catalog_cms->getProductRelatedByManufacturer($filter_data);
				
			} else {
				
					$parts = explode('_', (string)$this->request->get['path']);
					
					if(!empty($parts) && is_array($parts)) {
					
						$filter_data = array(
							'category_id'  => array_pop($parts),
							'limit' => $setting['limit']
						);
						
					$results = $this->model_catalog_cms->getProductRelatedByCategory($filter_data);
								
					}
			}
		
		$this->load->language('extension/module/featured_product');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');

					$data['language_id'] = $this->config->get('config_language_id');
					$data['button_instock'] = $this->language->get('button_instock');
					$data['fractal_name_sticker_product_new'] = $this->config->get('fractal_name_sticker_product_new');
					$data['fractal_name_sticker_product_top'] = $this->config->get('fractal_name_sticker_product_top');		
					$data['fractal_sticker_sale_product_auto'] = $this->config->get('fractal_sticker_sale_product_auto');	
					$data['fractal_sticker_new_product_auto'] = $this->config->get('fractal_sticker_new_product_auto');
					$data['fractal_sticker_product_new_day'] = $this->config->get('fractal_sticker_product_new_day');
					$data['fractal_sticker_product_top_rating'] = $this->config->get('fractal_sticker_product_top_rating');
					$data['fractal_sticker_product_top_viewed'] = $this->config->get('fractal_sticker_product_top_viewed');
					$data['fractal_sticker_product_top_ratinr'] = $this->config->get('fractal_sticker_product_top_ratinr');
					$data['fractal_sticker_top_product_auto'] = $this->config->get('fractal_sticker_top_product_auto');
					$data['fractal_limit_symbolst'] = $this->config->get('fractal_limit_symbolst');
					$data['aridiusinstock_status'] = $this->config->get('aridiusinstock_status');
					$data['aridius_qckview_status'] = $this->config->get('aridius_qckview_status');
					$data['fractal_attribute_see'] = $this->config->get('fractal_attribute_see');
                    $data['button_quickview'] = $this->language->get('button_quickview');
					$data['items'] = $setting['items'];
					$data['pag_speed'] = $setting['pag_speed'];
					$data['rew_speed'] = $setting['rew_speed'];
					$data['autoplay'] = $setting['autoplay'];
					$data['stophover'] = $setting['stophover'];
					$data['pagination'] = $setting['pagination'];
					$data['navigation'] = $setting['navigation'];
					$data['desc'] = $setting['desc'];
					$data['attribute'] = $setting['attribute'];
					$data['rat'] = $setting['rat'];
					$data['wish'] = $setting['wish'];
					$data['comp'] = $setting['comp'];
					$data['quickview'] = $setting['quickview'];
					$data['colleft'] = $setting['colleft'];
      
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();
		
		if (!empty($results)) {
			
			foreach ($results as $product) {

				if ($product) {
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product['special']) {
						$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product['rating'];
					} else {
						$rating = false;
					}
					
					$stickers = $this->getStickers($product['product_id']) ;

					$data['products'][] = array(

			'dateadded'  => $product['date_added'],
			'viewed'     => $product['viewed'],
			'stickers'   => $product['stickers'],
            'quantity'   => $product['quantity'],
		    'price_sticker'        => $product['price'],
			'special_sticker'      => (isset($product['special']) ? $product['special'] : false),								
			
						'product_id'  => $product['product_id'],
						'thumb'       => $image,
						'name'        => $product['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'sticker'     => $stickers,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'])
					);
				}
			}
		}
		

					$data['module'] = $module++;
      
		return $this->load->view('extension/module/featured_product', $data);

	}
	
	private function getStickers($product_id) {
	
 	$stickers = $this->model_catalog_product->getProductStickerbyProductId($product_id) ;	

		
		if (!$stickers) {
			return;
		}
		
		$data['stickers'] = array();
		
		foreach ($stickers as $sticker) {
			$data['stickers'][] = array(
				'position' => $sticker['position'],
				'image'    => HTTP_SERVER . 'image/' . $sticker['image']
			);		
		}
				
		return $this->load->view('product/stickers', $data);
	
	}
}