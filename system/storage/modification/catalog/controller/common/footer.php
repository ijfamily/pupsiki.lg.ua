<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_bestseller'] = $this->language->get('text_bestseller');
		$data['text_mostviewed'] = $this->language->get('text_mostviewed');
		$data['text_latest'] = $this->language->get('text_latest');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['bestseller'] = $this->url->link('product/bestseller');
		$data['mostviewed'] = $this->url->link('product/mostviewed');
		$data['latest'] = $this->url->link('product/latest');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

	                $this->load->language('extension/module/fractal');
	  		        $data['text_footer_contacts'] = $this->language->get('text_footer_contacts');
			        $data['text_footer_aboutus'] = $this->language->get('text_footer_aboutus');
					$data['text_footer_send'] = $this->language->get('text_footer_send');
					$data['text_footer_mailtext'] = $this->language->get('text_footer_mailtext');
	                $data['language_id'] = $this->config->get('config_language_id');
			        $data['fractal_top_links2'] = $this->config->get('fractal_top_links2');
					$data['fractal_telephoneon_footer'] = $this->config->get('fractal_telephoneon_footer');	
					$data['fractal_telephone1on_footer'] = $this->config->get('fractal_telephone1on_footer');	
					$data['fractal_telephone2on_footer'] = $this->config->get('fractal_telephone2on_footer');	
					$data['fractal_skype_footeron'] = $this->config->get('fractal_skype_footeron');	
					$data['fractal_mail_footeron'] = $this->config->get('fractal_mail_footeron');	
					$data['fractal_addres_footeron'] = $this->config->get('fractal_addres_footeron');	
					$data['fractal_time_footeron'] = $this->config->get('fractal_time_footeron');	
					$data['fractal_telephone_footer'] = $this->config->get('fractal_telephone_footer');
					$data['fractal_telephone1_footer'] = $this->config->get('fractal_telephone1_footer');
					$data['fractal_telephone2_footer'] = $this->config->get('fractal_telephone2_footer');
					$data['fractal_skype_footer'] = $this->config->get('fractal_skype_footer');
					$data['fractal_mail_footer'] = $this->config->get('fractal_mail_footer');
					$data['fractal_addres_footer'] = $this->config->get('fractal_addres_footer');
					$data['fractal_time_footer'] = $this->config->get('fractal_time_footer');
					$data['fractal_contact_footer'] = $this->config->get('fractal_contact_footer');	
					$data['fractal_return_footer'] = $this->config->get('fractal_return_footer');	
					$data['fractal_sitemap_footer'] = $this->config->get('fractal_sitemap_footer');	
					$data['fractal_manufacturer_footer'] = $this->config->get('fractal_manufacturer_footer');	
					$data['fractal_voucher_footer'] = $this->config->get('fractal_voucher_footer');	
					$data['fractal_affiliate_footer'] = $this->config->get('fractal_affiliate_footer');	
					$data['fractal_special_footer'] = $this->config->get('fractal_special_footer');
					$data['fractal_account_footer'] = $this->config->get('fractal_account_footer');
					$data['fractal_order_footer'] = $this->config->get('fractal_order_footer');
					$data['fractal_wishlist_footer'] = $this->config->get('fractal_wishlist_footer');
					$data['fractal_newsletter_footer'] = $this->config->get('fractal_newsletter_footer');
					$data['fractal_show_fluid_map_footer'] = $this->config->get('fractal_show_fluid_map_footer');
					$data['fractal_longitude_fluid_map'] = $this->config->get('fractal_longitude_fluid_map');					
					$data['fractal_latitude_fluid_map'] = $this->config->get('fractal_latitude_fluid_map');
					$data['fractal_description'] = $this->config->get('fractal_description');
					$data['fractal_show_contactblock'] = $this->config->get('fractal_show_contactblock');
					$data['fractal_fluid_map_zoom'] = $this->config->get('fractal_fluid_map_zoom');
					$data['fractal_license_text'] = $this->config->get('fractal_license_text');
					$data['fractal_counter'] = $this->config->get('fractal_counter');
					$data['fractal_description_map_logo'] = $this->config->get('fractal_description_map_logo');
					$data['fractal_fluid_map_key'] = $this->config->get('fractal_fluid_map_key');
					$data['fractal_show_info_marker_block'] = $this->config->get('fractal_show_info_marker_block');
					
					if ($this->request->server['HTTPS']) {
						$server = $this->config->get('config_ssl');
					} else {
						$server = $this->config->get('config_url');
					}
					if (is_file(DIR_IMAGE . $this->config->get('fractal_footer_imgico'))) {
						$data['map_img'] = $server . 'image/' . $this->config->get('fractal_footer_imgico');
					} else {
						$data['map_img'] = '';
					}
					if (is_file(DIR_IMAGE . $this->config->get('fractal_footer_pay'))) {
						$data['footer_pay'] = $server . 'image/' . $this->config->get('fractal_footer_pay');
					} else {
						$data['footer_pay'] = '';
					}
      

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}


                    $data['content_mail'] =  $this->load->controller('common/content_mail');
      
		return $this->load->view('common/footer', $data);
	}
}
