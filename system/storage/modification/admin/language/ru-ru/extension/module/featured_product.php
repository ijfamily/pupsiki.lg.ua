<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']    = 'Рекомендуемые товары в категории и производителе';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';
$_['text_help']        = 'Для работы модуля необходимо определить товары в редактировании категории или производителя';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_limit']      = 'Лимит';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

					$_['entry_autoplay']          = 'Автостарт прокрутки';
					$_['entry_items']             = 'Количество отображаемых';
					$_['entry_pag_speed']         = 'Скорость пагинации (мс.)';
					$_['entry_rew_speed']         = 'Скорость перемотки (мс.)';
					$_['entry_stophover']         = 'Пауза при наведении на товар';
					$_['entry_pagination']        = 'Отображать пагинацию';
					$_['entry_navigation']        = 'Отображать навигацию';
					$_['entry_desc']              = 'Отображать описание в товарах';
					$_['entry_attribute']         = 'Отображать атрибуты в товарах';
					$_['entry_rat']               = 'Отображать рейтинг в товарах';
					$_['entry_wish']              = 'Отображать "в закладки" в товарах';
					$_['entry_comp']              = 'Отображать "в сравнение" в товарах';
					$_['entry_quickview']         = 'Отображать "быстрый просмотр" в товарах';
					$_['entry_colleft']           = 'Отображать модуль в левой колонке';
      

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';
$_['error_name']       = 'Название должно содержать от 3 до 64 символов!';
$_['error_width']      = 'Укажите Ширину!';
$_['error_height']     = 'Укажите Высоту!';